locale: 'es'

header: true

pronouns:
    enabled: true
    default: 'él'
    any: 'any'
    plurals: false
    honorifics: false
    multiple:
        name: 'Formas intercambiables'
        description: 'Muchas personas no binarias usan más de una forma intercambiablemente y se sienten cómodas con cualquiera de las opciones.'
        examples: ['él&ella', 'él&elle', 'ella&elle']
    null: false
    emoji: false
    slashes: false
    avoiding: false
    others: 'Otros pronombres'

pronunciation:
    enabled: true
    voices:
        ES:
            language: 'es-ES'
            voice: 'Lucia'
            engine: 'standard'
        MX:
            language: 'es-MX'
            voice: 'Mia'
            engine: 'standard'

sources:
    enabled: true
    route: 'fuentes'
    submit: true
    mergePronouns: {}

nouns:
    enabled: true
    route: 'diccionario'
    collapsable: false
    plurals: false
    pluralsRequired: false
    declension: false
    submit: true
    templates: false
    inclusive:
        enabled: false
    terms:
        enabled: false

names:
    enabled: false

people:
    enabled: false

english:
    enabled: false

faq:
    enabled: true
    route: 'preguntas'

links:
    enabled: true
    route: 'enlaces'
    blogRoute: 'blog'
    links:
        -
            icon: 'comment-alt-edit'
            url: 'https://www.aacademica.org/1.congreso.internacional.de.ciencias.humanas/1500'
            headline: 'Fenómenos de convergencia y divergencia en el uso del lenguaje no binario'
            extra: ' – Descripción lingüística del uso de formas no binarias, en especial {/elle=elle/le}'
        -
            icon: 'comment-alt-edit'
            url: 'https://accedacris.ulpgc.es/handle/10553/73757'
            headline: 'El género no binario en la traducción al español: análisis del uso del lenguaje inclusivo no binario'
            extra: ' – Este trabajo aborda el uso de formas no binarias como resultado de traducciones del inglés al español.'
    mediaGuests:
        -
            icon: 'play-circle'
            url: 'https://open.spotify.com/episode/286zTXBHA0ChWNaWSwgFa0'
            headline: 'El lenguaje inclusivo de lxs polacxs – Ausir y Dante en un podcast <strong>Proyecto Polonia</strong>'
    mediaMentions: []
    socials:
        -
            icon: 'twitter'
            iconSet: 'b'
            url: 'https://twitter.com/PronounsPage'
            headline: '@PronounsPage'
    recommended: []

contact:
    enabled: true
    route: 'contacto'
    contacts:
        -
            icon: 'envelope'
            url: 'mailto:contact@pronouns.page'
            headline: 'contact@pronouns.page'
    team:
        enabled: true
        route: 'team'  # TODO
    blog: {}

support:
    enabled: true
    links:
        -
            icon: 'coffee'
            url: 'https://ko-fi.com/radajezykaneutralnego'
            headline: 'Ko-Fi'
        -
            icon: 'paypal'
            iconSet: 'b'
            url: 'https://paypal.me/RJNeutralnego'
            headline: 'PayPal'

user:
    enabled: true
    route: 'cuenta'
    termsRoute: 'terinos'

profile:
    enabled: true
    editorEnabled: true
    defaultWords:
        - ['señor', 'señora', 'señore', 'don', 'doña', 'doñe', 'usted', 'tú', 'vos']
        - ['persona', 'hombre', 'mujer', 'chico', 'chica', 'chique', 'muchach_', 'tip_', 'herman_', 'tí_', 'güey', 'pib_']
        - ['lind_', 'guap_', 'bonit_', 'mon_', 'hermos_', 'sexi']
        - ['pareja', 'novi_', 'marid_', 'espos_', 'media naranja']
    flags:
        defaultPronoun: 'ell_'

census:
    enabled: false

redirects: []

api:
    examples:
        pronouns_all: ['/api/pronouns']
        pronouns_one:
            - '/api/pronouns/ella'
            - '/api/pronouns/ella?examples[]=Creo%20que%20%7Bpronoun%7D%20es%20muy%20querid%7Binflection%7D.'

        sources_all: ['/api/sources']
        sources_one: ['/api/sources/01ET491ME8DPKNN1SV3NBK6KEA']

        nouns_all: ['/api/nouns']
        nouns_search: ['/api/nouns/search/esposa']
